var path = require('path')
, express = require('express')
, bodyParser = require('body-parser')
    , cookieParser = require('cookie-parser')
, app = express()
    // common
    app.set("views", __dirname + "/app/view");
    app.set("view engine", "pug");
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser("secret"));
    app.use(express.static(path.join(__dirname, "public")));
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
 

var index = require('./app/controller/index');
var db      = require("./database");
var controller = require("./app/controller/index");
var cors = require('cors');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) { 
    res.header('Access-Control-Allow-Origin', "*"); 
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE'); 
    res.header('Access-Control-Allow-Headers', 'Content-Type'); 
    next();
})
app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
db.connect(function(err){
    if(err){
        console.log('Exiting now!' + new Date());
        process.exit(1);
    }
    else{
        app.use('/',controller);
        var port = 3000;
        app.listen(port,function(){
            console.log('API service started at port '+port+' - '+new Date().getTime());
        });
    }
});
module.exports = app;
