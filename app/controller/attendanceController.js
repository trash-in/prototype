/**
 * @project trashin_prototype
 * @file   /app/model/attendancecontroller.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The attendanceController.js provides controller file for attendance.
 *
 *
 */

var attendanceModel = require('../model/attendance');
var utilFile = require('../../utils/utilities');

var attendanceController = function () { };

attendanceController.addEmployee = function (req, res) {
	if (req.body.employee_name) {
		utilFile.generateEmpId(function (error, id) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in creating the Id" });
			}
			else {
				var tempEmployee = new attendanceModel.schema;
				tempEmployee.employee_id = "EMP" + String(100000 + id + 1);
				tempEmployee.employee_name = String(req.body.employee_name);
				attendanceModel.save(tempEmployee, function (error, result) {
					if (error) {
						res.json({ "error": 1, "msg": "There was an error in saving the Employee" });
					}
					else {
						res.json({ "error": 0, "msg": "Successfully saved" });
					}

				});
			}
		});

	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:employee_name" });
	}
};

attendanceController.deleteEmployee = function (req, res) {
	if (req.body.employee_id) {
		attendanceModel.delete(req.body.employee_id, function (error, result) {
			if (error)
				res.json({ "error": 1, "msg": "There was an error in deleting the attendance" });
			else if (result.deletedCount == 0)
				res.json({ "error": 0, "msg": "Couldn't find the attendance to be deleted" });
			else {
				res.json({ "error": 0, "msg": "Successfully deleted" });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters attendanceController.deleteEmployee" });
	}
};
attendanceController.getEmployee = function (req, res) {
	attendanceModel.get(function (error, result) {
		if (error) {
			res.json({ "error": 1, "msg": "There was an error in fetching the employee" });
		}
		else if (!result)
			res.json({ "error": 0, "msg": "Couldn't find the employee to be fetched" });
		else {
			res.json({ "error": 0, "msg": "Successfully fetched", "employee": result});
		}
	});
};
attendanceController.getAttendance = function (req, res) {
	if (req.body.employee_id) {
		attendanceModel.getById(req.body.employee_id, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the attendance" });
			}
			else if (!result)
				res.json({ "error": 0, "msg": "Couldn't find the attendance to be fetched" });
			else {
				res.json({ "error": 0, "msg": "Successfully fetched", "attendance": result.attendance_value });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:mobile" });
	}
};


attendanceController.increaseAttendance = function (req, res) {
	if (req.body.employee_id) {
		attendanceModel.increaseAttendanceCount(req.body.employee_id, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the attendance" });
			}
			else if (result.result.nModified == 0) {
				res.json({ "error": 0, "msg": "Cant find employee" });
			}
			else {
				res.json({ "error": 0, "msg": "Successfully increased the attendance count for the employee_id" });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters: employee_id function attendanceController.increaseAttendance" });
	}
};

attendanceController.decreaseAttendance = function (req, res) {
	if (req.body.employee_id) {
		attendanceModel.decreaseAttendanceCount(req.body.employee_id, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the attendance" });
			}
			else if (result.result.nModified == 0) {
				res.json({ "error": 0, "msg": "Cant find employee" });
			}
			else {
				res.json({ "error": 0, "msg": "Successfully decreased the attendance count for the employee_id" });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters: employee_id function attendanceController.decreaseAttendance" });
	}
};

module.exports = attendanceController;
