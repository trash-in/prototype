/**
 * @project trashin_prototype
 * @file   /app/model/dailyEntryController.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The dailyEntryController.js provides schema of a node named dailyEntryController.
 *
 *
 */

 var dailyEntryModel = require('../model/dailyEntry');
 var utilFile = require('../../utils/utilities');
 var dailyEntryController = function(){};

 dailyEntryController.addDailyEntry = function(req,res){
 	if(req.body.type&& req.body.qty && req.body.per_id){
 		var tempDailyEntry = new dailyEntryModel.schema;
 		tempDailyEntry._id= utilFile.generateId();
 		tempDailyEntry.type = req.body.type;
		tempDailyEntry.qty = req.body.qty;
		tempDailyEntry.per_id = String(req.body.per_id);
 		dailyEntryModel.save(tempDailyEntry,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in saving the dailyEntry"});
 			}
 			else{
 				res.json({"error":0,"msg":"Successfully saved"});
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}
 };

 dailyEntryController.deleteDailyEntry = function(req,res){
 	if(req.body.id){ 		
 		dailyEntryModel.delete(req.body.id,function(error,result){
 			if(error)
 				res.json({"error":1,"msg":"There was an error in deleting the dailyEntry"});
 			else if(result.deletedCount==0)
 				res.json({"error":0,"msg":"Couldn't find the dailyEntry to be deleted"});
 			else{
				res.json({"error":0,"msg":"Successfully deleted"});
  			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});	
 	}
 };

 dailyEntryController.getDailyEntry = function(req,res){
 	if(req.body.per_id){
 		dailyEntryModel.get(req.body.per_id,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the dailyEntry"});	
 			}
 			else if(result.length==0)
 				res.json({"error":0,"msg":"Couldn't find the dailyEntry to be fetched"});
 			else{
 				res.json({"error":0,"msg":"Successfully fetched","obj":result});
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters:mobile"});	
 	}
 }

module.exports = dailyEntryController;