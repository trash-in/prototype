 var dealController = function(){};
 var utilFile = require('../../utils/utilities');
 var dealModel = require('../model/deal');
 var saleModel = require('../model/sale');
 dealController.addDeal = function(req,res){
 	if(req.body.qty&& req.body.amt_tot && req.body.amt_due && req.body.type&&req.body.per_id&&req.body.trg_id){
 		var tempDeal = new dealModel.schema;
 		tempDeal._id= utilFile.generateId();
 		tempDeal.qty = Number(req.body.qty);
 		tempDeal.amt_tot = String(req.body.amt_tot);
		tempDeal.amt_due = String(req.body.amt_due);
 		tempDeal.per_id = String(req.body.per_id);
 		tempDeal.trg_id = String(req.body.trg_id);
		tempDeal.per_name = String(req.body.per_name);
 		tempDeal.trg_name = String(req.body.trg_name);
 		tempDeal.type = req.body.type;
 		dealModel.save(tempDeal,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in saving the deal"});
 			}
 			else{
				var tempSale = new saleModel.schema;
				tempSale._id= tempDeal._id;
				tempSale.qty = Number(req.body.qty);
				tempSale.amt_tot = String(req.body.amt_tot);
				tempSale.amt_rec = String(req.body.amt_due);
				tempSale.per_id = String(req.body.trg_id);
				tempSale.trg_id = String(req.body.per_id);
				tempSale.per_name = String(req.body.trg_name);
 				tempSale.trg_name = String(req.body.per_name);
				tempSale.type = req.body.type;
				saleModel.save(tempSale,function(error,result){
					if(error){
						res.json({"error":1,"msg":"There was an error in saving the sale"});
					}
					else{
						res.json({"error":0,"msg":"Successfully saved"});
 					}
 				});
 			}
		 });
	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}
 };

 dealController.getDealByPer= function(req,res){
 	if(req.body.per_id){
 		dealModel.get(req.body.per_id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the deal"});
 			}
 			else if(obj.length==0){
 				res.json({"error":0,"msg":"There are no deals to show"});
 			}
 			else{
 				res.json({"error":0,"obj":obj});
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}

 }
 dealController.deleteDeal = function(req,res){
 	if(req.body.id){
 		dealModel.delete(req.body.id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the deal"});
 			}
 			else if(obj.deletedCount==0){
 				res.json({"error":0,"msg":"There are no deals to delete"});
 			}
 			else{
 				saleModel.delete(req.body.id,function(error,obj){
					if(error){
						res.json({"error":1,"msg":"There was an error in fetching the sale"});
					}
					else if(obj.deletedCount==0){
						res.json({"error":0,"msg":"There are no sales to delete"});
					}
					else{
						res.json({"error":0,"msg":"Successfully deleted"});
					}
				 });
			 }
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}

 }

module.exports= dealController;