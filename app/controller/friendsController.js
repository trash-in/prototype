/**
 * @project trashin_prototype
 * @file   /app/model/rating.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The rating.js provides schema of a node named rating.
 *
 *
 */
 var personModel = require('../model/person');
 var friendsModel = require('../model/friends');
 var utilFile = require('../../utils/utilities');
 
 
 var friendsController = function(){};

 friendsController.addFriendForce = function(req,res){
 	if(req.body.id&&req.body.mobile&& req.body.name){
 		var tempPerson = new personModel.schema;
 		tempPerson._id = utilFile.generateId();
 		tempPerson.mobile= req.body.mobile;
 		tempPerson.name = req.body.name;
 		tempPerson.ptype = req.body.ptype;
 		personModel.save(tempPerson,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in saving the person"});
 				return;
 			}
 			else{
 				var tempObj={
 					"src":req.body.id,
 					"trg":result.ops[0]._id
 				};	
 				friendsModel.save({_id:tempObj},function(error,obj){
 					if(error){
 						res.json({"error":1,"msg":"There was an error in saving the rating"});
 						return;
 					}
					else{
						res.json({"error":0,"msg":"Successfully added"});
					} 					

 				});
			} 						
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters id,mobile,"});
 	}
 };

 friendsController.sendRequest= function(req,res){
 	if(req.body.id&&req.body.tid){
 		personModel.sendRequest(req.body.tid,req.body.id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in sending the request"});
				return;
 			}
 			else if(obj.nMatched==0){
 				res.json({"error":1,"msg":"There was an error in sending the request,because the person is not in the datebase"});
				return;
 			}
 			else{
 				res.json({"error":0,"msg":"Successfully sent the request"});
 			}

 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters id"});
 	}
 }
 friendsController.getFriends = function(req,res){
 	if(req.body.id){
 		friendsModel.getPersons(req.body.id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in getting the friends","obj":[]});
				return;
 			}
 			else if(obj.length==0){
 				res.json({"error":1,"msg":"There are no friends for the given user","obj":[]});
				return;
 			}
 			else{
 				res.json({"error":0,"obj":obj});
				return;
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters id"});
		return;
 	}

 };

 friendsController.deleteFriend = function(req,res){
 	if(req.body.sid&&req.body.tid){
 		var tempObj={
 			"src":req.body.sid,
 			"trg":req.body.tid
 		};
 		var tempObj1={
 			"src":req.body.tid,
 			"trg":req.body.sid
 		};	
 		friendsModel.delete(tempObj,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in deleting the friendship"});
				return;
 			}
 			else if(obj.deletedCount==0){
 				friendsModel.delete(tempObj1,function(error,obj){
 					if(error){
 						res.json({"error":1,"msg":"There was an error in deleting the friendship"});
						return;
 					}
 					else if(obj.deletedCount==0){
 						res.json({"error":0,"msg":"Couldnt delete, as there was no friend object to delete"});
 					}
 					else{
 						es.json({"error":0,"msg":"Successfully deleted"});
 					}
 				});
 			}
 			else{
 				friendsModel.delete(tempObj1,function(error,obj){
 					if(error){
 						res.json({"error":1,"msg":"There was an error in deleting the friendship"});
						return;
 					}
 					else{
 						res.json({"error":0,"msg":"Successfully deleted"});
 					}
 				});
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters source and target ids"});
 	}
 }
 module.exports = friendsController;
