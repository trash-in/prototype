var express = require('express');
var router = express.Router();
var person = require('./personController');
var rating = require('./ratingController');
var friends = require('./friendsController');
var attendance = require('./attendanceController');
var deal = require('./dealController');
var sale = require('./saleController');
var dailyEntry = require('./dailyEntryController');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/addPerson',person.addPerson);

router.post('/deletePerson',person.deletePerson);

router.post('/getPerson',person.getPerson);

router.post('/getAll',person.getAll);

router.post('/getPersonsById',person.getPersonsById);

router.post('/login',person.login);

router.post('/addRating',rating.addRating);

router.post('/getRatingBySrc',rating.getRatingBySrc);

router.post('/getRatingByTrg',rating.getRatingByTrg);

router.post('/deleteRating',rating.deleteRating);

router.post('/addFriendForce',friends.addFriendForce);

router.post('/sendRequest',friends.sendRequest);

router.post('/acceptRequest', person.acceptRequest);

router.post('/getFriends',friends.getFriends);

router.post('/deleteFriend',friends.deleteFriend);

router.post('/addEmployee', attendance.addEmployee);

router.post('/deleteEmployee', attendance.deleteEmployee);

router.post('/getAttendance', attendance.getAttendance);

router.post('/getEmployee', attendance.getEmployee);

router.post('/increaseAttendance', attendance.increaseAttendance);

router.post('/decreaseAttendance', attendance.decreaseAttendance);

router.post('/addDeal', deal.addDeal);

router.post('/getDealByPer',deal.getDealByPer);

router.post('/deleteDeal',deal.deleteDeal);

router.post('/addSale',sale.addSale);

router.post('/getSaleByPer',sale.getSaleByPer);

router.post('/deleteSale',sale.deleteSale);

router.post('/addDailyEntry',dailyEntry.addDailyEntry);

router.post('/getDailyEntry',dailyEntry.getDailyEntry);

router.post('/deleteDailyEntry',dailyEntry.deleteDailyEntry);

router.post('/toggleIsReady',person.toggleIsReady);

module.exports = router;
