/**
 * @project trashin_prototype
 * @file   /app/model/person.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The person.js provides schema of a node named person.
 *
 *
 */

var personModel = require('../model/person');
var friendsModel = require('../model/friends');
var passwordModel = require('../model/password');
var crypto = require('crypto');
var utilFile = require('../../utils/utilities');
var personController = function () { };
var db = require('../../database');
var coll = require('../../database/collections');

personController.addPerson = function (req, res) {
	if (req.body.name && req.body.mobile && req.body.ptype && req.body.password) {
		var tempPerson = new personModel.schema;
		tempPerson._id = utilFile.generateId();
		tempPerson.name = String(req.body.name);
		tempPerson.mobile = String(req.body.mobile);
		tempPerson.email = String(req.body.email);
		tempPerson.ptype = req.body.ptype;
		if (req.body.city)
			tempPerson.city = req.body.city;
		if (req.body.buy_type)
			tempPerson.buy_type = req.body.buy_type;
		if (req.body.sell_type)
			tempPerson.sell_type = req.body.sell_type;
		personModel.save(tempPerson, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in saving the person" });
			}
			else {
				var tempPass = new passwordModel.schema;
				tempPass.mobile = req.body.mobile;
				tempPass.password = crypto.createHash('sha256').update(req.body.password).digest('base64');
				passwordModel.save(tempPass, function (error, obj) {
					if (error)
						res.json({ "error": 1, "msg": "There was an error in saving the password" });
					else
						res.json({ "error": 0, "msg": "Successfully saved" });
				});
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters" });
	}
};

personController.deletePerson = function (req, res) {
	if (req.body.mobile) {
		personModel.delete(req.body.mobile, function (error, result) {
			if (error)
				res.json({ "error": 1, "msg": "There was an error in deleting the person" });
			else if (result.deletedCount == 0)
				res.json({ "error": 0, "msg": "Couldn't find the person to be deleted" });
			else {
				passwordModel.delete(req.body.mobile, function (error, result) {
					if (error)
						res.json({ "error": 1, "msg": "There was an error in deleting the password" });
					else
						res.json({ "error": 0, "msg": "Successfully deleted" });
				});
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters" });
	}
};
personController.getPersonsById = function (req, res) {
	if (req.body.ids) {
		personModel.getByIds(req.body.ids, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the person" });
			}
			else if (result.length == 0)
				res.json({ "error": 0, "msg": "Couldn't find the person to be fetched", "obj": [] });
			else {
				var fin = [];
				for (var i = 0; i < result.length; i++) {
					tmp = {};
					tmp.names = result[i].name;
					tmp.mobs = result[i].mobile;
					fin.push(tmp);
				}
				res.json({ "error": 0, "msg": "Successfully fetched", "obj": fin });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:mobile" });
	}
}
personController.getPerson = function (req, res) {
	if (req.body.mobile) {
		personModel.get(req.body.mobile, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the person" });
			}
			else if (result.length == 0)
				res.json({ "error": 0, "msg": "Couldn't find the person to be fetched", "obj": [] });
			else {
				res.json({ "error": 0, "msg": "Successfully fetched", "obj": result[0] });
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:mobile" });
	}
}
personController.login = function (req, res) {
	if (req.body.mobile&&req.body.password) {
		personModel.login(req.body.mobile,function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the person" });
			}
			else {
				if(result&&result.password==crypto.createHash('sha256').update(req.body.password).digest('base64')){
					res.json({ "error": 0, "msg": "IN" });
				}
				else{
					res.json({ "error": 1, "msg": "OUT" });
				}
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:mobile" });
	}
}
personController.getAll = function (req, res) {
	if (req.body.id) {
		personModel.getAll(req.body.id,function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the person" });
			}
			else if (result.length == 0)
				res.json({ "error": 0, "msg": "Couldn't find the person to be fetched", "obj": [] });
			else {
				res.json({ "error": 0, "msg": "Successfully fetched", "obj": result });
			}
		},req.body.ptype||'',req.body.flag||'');
	}
	else{
		res.json({ "error": 1, "msg": "Please pass the required parameters:id" });
	}
}

personController.acceptRequest = function (req, res) {
	if (req.body.id && req.body.tid) {
		var tempObj = {
			"src": req.body.id,
			"trg": req.body.tid
		}
		var tempObj1 = {
			"src": req.body.tid,
			"trg": req.body.id
		}
		friendsModel.save({ _id: tempObj }, function (error, obj) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in adding the friend" });
			}
			else {
				friendsModel.save({ _id: tempObj1 }, function (error, obj) {
					if (error) {
						res.json({ "error": 1, "msg": "There was an error in adding the friend" });
					}
					else {
						personModel.pop(req.body.tid, req.body.id, function (error, obj) {
							if (error)
								res.json({ "error": 1, "msg": "There was an error in popping the friend" });
							else {
								res.json({ "error": 0, "msg": "Added friend" });
							}
						});
					}

				});
			}
		});
	}
	else {
		res.json({ "error": 1, "msg": "Please pass the required parameters:Source and Target id" });
	}
}

personController.toggleIsReady = function (req, res) {
	if (req.body.mobile) {
		personModel.get(req.body.mobile, function (error, result) {
			if (error) {
				res.json({ "error": 1, "msg": "There was an error in fetching the person" });
			}
			else if (result.length == 0)
				res.json({ "error": 0, "msg": "Couldn't find the person to be fetched", "obj": [] });
			else {
				var is_ready = result[0].is_ready;
				db.get().collection(coll.person).update({"mobile": req.body.mobile }, { $set: { "is_ready": !is_ready } }, function (error, obj) {
					var fin=result[0];
					fin.is_ready=!fin.is_ready;
					res.json({ "error": 0, "msg": "Successfully fetched", "obj": fin });
				});

			}
		});
	}
	else
	{
		console.log("mobile number not sent");
	}
}
module.exports = personController;