/**
 * @project trashin_prototype
 * @file   /app/model/rating.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The rating.js provides schema of a node named rating.
 *
 *
 */

 var ratingModel = require('../model/rating');
 //var passwordModel = require('../model/password');
// var crypto = require('crypto');

 var ratingController = function(){};

 ratingController.addRating = function(req,res){
 	if(req.body.rating&& req.body.review && req.body.src&& req.body.trg){
 		var tempRating = new ratingModel.schema;
 		tempRating.rating = Number(req.body.rating);
 		tempRating.review = String(req.body.review);
 		var tempObj = {
 			"src": req.body.src,
 			"trg": req.body.trg
 		}
 		tempRating._id = tempObj;
 		ratingModel.save(tempRating,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in saving the rating"});
 			}
 			else
 			 	res.json({"error":0,"msg":"Successfully Added"}); 						
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters rating, review, id"});
 	}
 };

 ratingController.deleteRating = function(req,res){
 	if(req.body.src&& req.body.trg){
 		var tempObj={
 			"src":req.body.src,
 			"trg":req.body.trg
 		} 		
 		ratingModel.delete(tempObj,function(error,result){
 			if(error)
 				res.json({"error":1,"msg":"There was an error in deleting the rating"});
 			else if(result.deletedCount==0)
 				res.json({"error":0,"msg":"Couldn't find the rating to be deleted"});
 			else{
 				ratingModel.delete(req.body.id,function(error,result){
 					if(error)
 						res.json({"error":1,"msg":"There was an error in deleting the rating"});
 					else
 						res.json({"error":0,"msg":"Successfully deleted"});
 				});
  			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters, id of the rating"});	
 	}
 };

 ratingController.getRatingByTrg = function(req,res){
 	if(req.body.id){
 		ratingModel.getByTrg(req.body.id,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the rating"});	
 			}
 			else if(result.length==0)
 				res.json({"error":0,"msg":"Couldn't find the rating to be fetched"});
 			else{
 				res.json({"error":0,"msg":"Successfully fetched","obj":result});
 			}
 		});
 	}
 }

 ratingController.getRatingBySrc = function(req,res){
 	if(req.body.id){
 		ratingModel.getBySrc(req.body.id,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the rating"});	
 			}
 			else if(result.length==0)
 				res.json({"error":0,"msg":"Couldn't find the rating to be fetched"});
 			else{
 				res.json({"error":0,"msg":"Successfully fetched","obj":result});
 			}
 		});
 	}
 }
 module.exports = ratingController;
