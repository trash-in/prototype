 /**
 * @project trashin_prototype
 * @file   /app/model/saleController.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The saleController.js provides controller file for saleController.
 *
 *
 */
 var saleController = function(){};
 var utilFile = require('../../utils/utilities');
 var saleModel = require('../model/sale');
 var dealModel = require('../model/deal');
 
 saleController.addSale = function(req,res){
 	if(req.body.qty&& req.body.amt_tot && req.body.amt_due && req.body.type&&req.body.per_id&&req.body.trg_id){
 		var tempSale = new saleModel.schema;
 		tempSale._id= utilFile.generateId();
 		tempSale.qty = Number(req.body.qty);
 		tempSale.amt_tot = String(req.body.amt_tot);
		tempSale.amt_rec = String(req.body.amt_due);
 		tempSale.per_id = String(req.body.per_id);
 		tempSale.trg_id = String(req.body.trg_id);
		tempSale.per_name = String(req.body.per_name);
		tempSale.trg_name = String(req.body.trg_name);
 		tempSale.type = req.body.type;
 		saleModel.save(tempSale,function(error,result){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in saving the sale"});
 			}
 			else{
                var tempDeal = new dealModel.schema;
                tempDeal._id= tempSale._id;
                tempDeal.qty = Number(req.body.qty);
                tempDeal.amt_tot = String(req.body.amt_tot);
                tempDeal.amt_due = String(req.body.amt_due);
				tempDeal.per_name = String(req.body.trg_name);
 				tempDeal.trg_name = String(req.body.per_name);
                tempDeal.per_id = String(req.body.trg_id);
                tempDeal.trg_id = String(req.body.per_id);
                tempDeal.type = req.body.type;
                dealModel.save(tempDeal,function(error,result){
                    if(error){
 				        res.json({"error":1,"msg":"There was an error in saving the deal"});
 			        }
                    else{
                        res.json({"error":0,"msg":"Successfully saved"});
 				    }
                });
            }
        });
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}
 };

 saleController.getSaleByPer= function(req,res){
 	if(req.body.per_id){
 		saleModel.get(req.body.per_id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the sale"});
 			}
 			else if(obj.length==0){
 				res.json({"error":0,"msg":"There are no sales to show"});
 			}
 			else{
 				res.json({"error":0,"obj":obj});
 			}
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}

 }
 saleController.deleteSale = function(req,res){
 	if(req.body.id){
 		saleModel.delete(req.body.id,function(error,obj){
 			if(error){
 				res.json({"error":1,"msg":"There was an error in fetching the sale"});
 			}
 			else if(obj.deletedCount==0){
 				res.json({"error":0,"msg":"There are no sales to delete"});
 			}
 			else{
                dealModel.delete(req.body.id,function(error,obj){
 			        if(error){
 				        res.json({"error":1,"msg":"There was an error in fetching the deal"});
 			        }
 			        else if(obj.deletedCount==0){
 				        res.json({"error":0,"msg":"There are no deals to delete"});
 			        }
                    else{
                        res.json({"error":0,"msg":"Successfully deleted"});
                    }
 		        });
            }
 		});
 	}
 	else{
 		res.json({"error":1,"msg":"Please pass the required parameters"});
 	}
 }

module.exports= saleController