/**
 * @project trashin_prototype
 * @file   /app/model/attendance.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The attendance.js provides schema of a node named for attendance of the employee
 *
 *
 */

//schema
var db = require('../../database');
var coll = require('../../database/collections');

var attendance = function(){};

attendance.schema = function(){
	this.employee_id = null;
	this.employee_name = null;
	this.attendance_value = 0;
};

/**
 * Save attendance details
/**
 * Save attendance details
 * @method save
 * @param obj - attendance object, instance of attendance.schema
 * @param callback
 */
attendance.save = function(obj,cb){
    if(obj){
        db.get().collection(coll.attendance).insertOne(obj,function(err,result){
            cb(err,result);
        });
    }

};

/**
 * Fetch attendance details
 * @method get
 * @param id
 * @param callback
 */

attendance.getById = function(employee_id,cb){
    if(employee_id){
        db.get().collection(coll.attendance).findOne({"employee_id":employee_id},function(error,result){
            cb(error,result);
        });
    }
}
attendance.get = function(cb){
		db.get().collection(coll.attendance).find({}).toArray(function(error,result){
            cb(error,result);
        });
}

/**
 * Delete attendance by employee_id
 * @method delete
 * @param id
 * @param callback
 */
attendance.delete = function(employee_id,cb){
    //validate fields before saving
    db.get().collection(coll.attendance).deleteOne({employee_id: employee_id},function(err,result){
        cb(err,result);
    });
};


/**
 * Increase attendance count for an employee for a particular day
 * @method increaseAttendanceCount
 * @param employee_id
 * @param callback
*/
attendance.increaseAttendanceCount = function(employee_id, cb){
	if(employee_id){
		db.get().collection(coll.attendance).update(
			{employee_id : employee_id},
			{$inc:{attendance_value:1} },
			function(err, result){ cb(err, result); }
		)
	}
};

/**
 * Decrease attendance count for an employee for a particular day
 * @method decreaseAttendanceCount
 * @param employee_id
 * @param callback
*/
attendance.decreaseAttendanceCount = function(employee_id, cb){
	if(employee_id){
		db.get().collection(coll.attendance).update(
			{employee_id : employee_id},
			{$inc:{attendance_value:-1} },
			function(err, result){ cb(err, result); }
		)
	}
};

module.exports = attendance;
