/**
 * @project trashin_prototype
 * @file   /app/model/dailyEntry.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The dailyEntry.js provides schema of a node named dailyEntry.
 *
 *
 */
 
//schema
var db = require('../../database');
var coll = require('../../database/collections');
 
var dailyEntry = function(){};
 
dailyEntry.schema = function(){
    this._id = String;
    this.type = Array;
    this.qty = Array;
    this.per_id = String;
    this.timestamp = new Date();
};
 
/**
 * Save dailyEntry details
 * @method save
 * @param obj - dailyEntry object, instance of dailyEntry.schema
 * @param callback
 */
dailyEntry.save = function(obj,cb){
    if(obj){
        db.get().collection(coll.dailyentry).insertOne(obj,function(err,result){
            cb(err,result);
        });
    }
 
};
dailyEntry.update = function(id,obj,cb){
    db.get().collection(coll.dailyentry).update({"_id":id},{$addToSet: robj, $set:qobj},{upsert:true,insert:false,multi:true},function(err,result){
        cb(err,result);
            });
};
/**
 * Fetch dailyEntry details
 * @method get
 * @param id
 * @param callback
 */
dailyEntry.get = function(per_id,cb){
    if(per_id){
        db.get().collection(coll.dailyentry).find({"per_id":per_id}).toArray(function(error,result){
            cb(error,result);
        });
    }
};
/**
 * Delete dailyEntry by id
 * @method delete
 * @param id
 * @param callback
 */
dailyEntry.delete = function(id,cb){
    //validate fields before saving
    db.get().collection(coll.dailyentry).deleteOne({_id:id},function(err,result){
        cb(err,result);
    });
};

module.exports = dailyEntry;
