var db = require('../../database');
var coll = require('../../database/collections');
 
var deal = function(){};
 
deal.schema = function(){
    this._id = String;
    this.qty = Number;
    this.per_id=String;
    this.trg_id=String;
    this.per_name=String;
    this.trg_name=String;
    this.amt_tot = Number;
    this.amt_due = Number;
    this.type = String;
    this.is_sorted= false;
    this.seg_type=Object;
};
deal.save = function(obj,cb){
	db.get().collection(coll.deal).insertOne(obj,function(err,result){
        cb(err,result);
    });
    
};
deal.get = function(per_id,cb){
    db.get().collection(coll.deal).find({"per_id":per_id}).toArray(function(error,result){
        cb(error,result);
    });
};
deal.update = function(id,obj,cb){
    db.get().collection(coll.deal).update({"_id":id},{$set:qobj},{upsert:true,insert:false,multi:true},function(err,result){
        cb(err,result);
    });
};
deal.delete = function(id,cb){
    //validate fields before saving
    db.get().collection(coll.deal).deleteOne({_id:id},function(err,result){
        cb(err,result);
    });
};

module.exports= deal;
