/**
 * @project trashin_prototype
 * @file   /app/model/person.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The person.js provides schema of a node named person.
 *
 *
 */

var db = require('../../database');
var coll = require('../../database/collections');

var friends = function () { }

friends.schema = function () {
    this._id = {
        src: null, trg: null
    };
    this.updated_at = new Date().getTime();

}
friends.save = function (obj, cb) {
    if (obj) {
        db.get().collection(coll.friends).insertOne(obj, function (err, result) {
            cb(err, result);
        });
    }
}

friends.get = function (id, cb) {
    if (id) {
        db.get().collection(coll.friends).find({ "_id": id }).toArray(function (error, result) {
            cb(error, result);
        });
    }
};
friends.getPersons = function (id, cb) {
    if (id) {
        var fin = [];
        db.get().collection(coll.friends).find({ "_id.src": id }).toArray(function (error, doc) {
            if (error)
                cb(true, []);
            else if (doc.length == 0)
                cb(false, []);
            else {
                for (var i = 0; i < doc.length; i++) {
                    fin.push(doc[i]._id.trg);
                }
                db.get().collection(coll.person).find({ "_id": { $in: fin } }).toArray(function (error, obj) {
                    cb(error, obj);
                });
            }
        });
    }
};
friends.delete = function (id, cb) {
    db.get().collection(coll.friends).deleteOne({ _id: id }, function (err, result) {
        cb(err, result);
    });
}

module.exports = friends;