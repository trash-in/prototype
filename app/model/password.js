/**
 * @project trashin_prototype
 * @file   /app/model/password.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The password.js provides schema of a node named password.
 *
 *
 */
 
//schema
var db = require('../../database');
var coll = require('../../database/collections');
 
var password = function(){};
 
password.schema = function(){
    this._id = Number;
    this.mobile = String;
    this.password= String;
};
 
/**
 * Save password details
 * @method save
 * @param obj - password object, instance of password.schema
 * @param callback
 */
password.save = function(obj,cb){
    if(obj){
        db.get().collection(coll.password).insertOne(obj,function(err,result){
            cb(err,result);
        });
    }
 
};
password.update = function(id,password,cb){
    db.get().collection(coll.password).update({"mobile":id},{"password":password},function(err,result){
        cb(err,result);
            });
};
 
 
/**
 * Fetch password details
 * @method get
 * @param id
 * @param callback
 */
password.get = function(mob,cb){
    if(mob){
        db.get().collection(coll.password).find({"mobile":mob}).toArray(function(error,result){
            cb(error,result);
        });
    }
};

/**
 * Delete password by id
 * @method delete
 * @param id
 * @param callback
 */
password.delete = function(mobile,cb){
    //validate fields before saving
    db.get().collection(coll.password).deleteOne({mobile:mobile},function(err,result){
        cb(err,result);
    });
};
module.exports = password;
