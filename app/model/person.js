/**
 * @project trashin_prototype
 * @file   /app/model/person.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The person.js provides schema of a node named person.
 *
 *
 */

//schema
var db = require('../../database');
var coll = require('../../database/collections');

var person = function () { };

person.schema = function () {
    this._id = String;
    this.name = String;
    this.dp = String;
    this.city = String;
    this.mobile = String;
    this.ptype = Array;
    this.buy_type = Array;
    this.sell_type = Array;
    this.is_ready = false;
    this.requests = [];
};

/**
 * Save person details
 * @method save
 * @param obj - person object, instance of person.schema
 * @param callback
 */
person.save = function (obj, cb) {
    if (obj) {
        db.get().collection(coll.person).insertOne(obj, function (err, result) {
            cb(err, result);
        });
    }

};
person.update = function (id, obj, cb) {
    db.get().collection(coll.person).update({ "_id": id }, { $addToSet: robj, $set: qobj }, { upsert: true, insert: false, multi: true }, function (err, result) {
        cb(err, result);
    });
};

person.pop= function(id,fid,cb){
    db.get().collection(coll.person).update({ "_id": id }, { $pull: {"requests":fid }}, { upsert: true, insert: false, multi: true }, function (err, result) {
        cb(err, result);
    });
}

/**
 * Fetch person details
 * @method get
 * @param id
 * @param callback
 */
person.getAll = function (id,cb,type,flag) {
    if(type!=''&&flag!=''){
        type=Number(type);
        flag=Number(flag);
        db.get().collection(coll.friends).find({ "_id.src": id }).toArray(function (error, result) {
            var arr=[];
            for(var i=0;i<result.length;i++){
                arr.push(result[i]._id.trg);
            }
            arr.push(id);
            db.get().collection(coll.person).find({$and:[{_id:{$nin:arr}},{ptype:String(type+flag)}]}).toArray(function (error, result1) {
                cb(error, result1);
            });
        });
    }
    else{
        db.get().collection(coll.person).find({_id:{$nin:[id]}}).toArray(function (error, result) {
            cb(error, result);
        });
    }
};
person.get = function (mob, cb) {
    if (mob) {
        db.get().collection(coll.person).find({ "mobile": mob }).toArray(function (error, result) {
            cb(error, result);
        });
    }
};
person.login = function (mob,cb) {
    db.get().collection(coll.password).findOne({ "mobile": mob },function (error, result) {
        cb(error, result);
    });
};
person.getByIds = function (arr, cb) {
    db.get().collection(coll.person).find({ _id :{$in : arr }}).toArray(function (err, result) {
        cb(err, result);
    });

};

/**
 * Delete person by id
 * @method delete
 * @param id
 * @param callback
 */
person.delete = function (mobile, cb) {
    //validate fields before saving
    db.get().collection(coll.person).deleteOne({ mobile: mobile }, function (err, result) {
        cb(err, result);
    });
};

person.sendRequest = function (id, sid, cb) {
    db.get().collection(coll.person).update({ _id: id }, { $addToSet: { "requests": sid } }, function (error, obj) {
        cb(error, obj);
    });
}
module.exports = person;
