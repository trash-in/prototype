/**
 * @project trashin_prototype
 * @file   /app/model/person.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The person.js provides schema of a node named person.
 *
 *
 */

//schema
var db = require('../../database');
var coll = require('../../database/collections');

var rating = function(){};

rating.schema = function(){
	this._id = {
		src: null , trg: null
	};
	this.updated_at = new Date().getTime();
	this.rating = null;
	this.review = null;
};

/**
 * Save rating details
/**
 * Save rating details
 * @method save
 * @param obj - rating object, instance of rating.schema
 * @param callback
 */
rating.save = function(obj,cb){
    if(obj){
        db.get().collection(coll.rating).insertOne(obj,function(err,result){
            cb(err,result);
        });
    }

};

/**
 * Fetch rating details
 * @method get
 * @param id
 * @param callback
 */
rating.getByTrg = function(id,cb){
    if(id){
        db.get().collection(coll.rating).find({"_id.trg":id}).toArray(function(error,result){
            cb(error,result);
        });
    }
};

rating.getBySrc = function(id,cb){
    if(id){
        db.get().collection(coll.rating).find({"_id.src":id}).toArray(function(error,result){
            cb(error,result);
        });
    }
}

/**
 * Delete rating by id
 * @method delete
 * @param id
 * @param callback
 */
rating.delete = function(id,cb){
    //validate fields before saving
    db.get().collection(coll.rating).deleteOne({_id:id},function(err,result){
        cb(err,result);
    });
};
module.exports = rating;

