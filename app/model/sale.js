var db = require('../../database');
var coll = require('../../database/collections');
 
var sale = function(){};
 
sale.schema = function(){
    this._id = String;
    this.qty = Number;
    this.per_id=String;
    this.trg_id=String;
    this.per_name=String;
    this.trg_name=String;
    this.amt_tot = Number;
    this.amt_rec = Number;
    this.type = String;
    this.is_sorted= false;
    this.seg_type=Object;
};
sale.save = function(obj,cb){
	db.get().collection(coll.sale).insertOne(obj,function(err,result){
        cb(err,result);
    });
    
};
sale.get = function(per_id,cb){
    db.get().collection(coll.sale).find({"per_id":per_id}).toArray(function(error,result){
        cb(error,result);
    });
};
sale.update = function(id,obj,cb){
    db.get().collection(coll.sale).update({"_id":id},{$set:qobj},{upsert:true,insert:false,multi:true},function(err,result){
        cb(err,result);
    });
};
sale.delete = function(id,cb){
    //validate fields before saving
    db.get().collection(coll.sale).deleteOne({_id:id},function(err,result){
        cb(err,result);
    });
};

module.exports= sale;
