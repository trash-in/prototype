var collections = {
    person: 'person', // used for storing person data
    password : 'password', //used for storing passwords of users
    rating: 'rating' ,//used to store ratings
    friends: 'friends', //used to store friends ie trusted sources   
    attendance: 'attendance',  //used to add employee and inc/dec his attendance
    counters: 'counters', //used to count and help in autoincrement fields
    deal:'deal', //used to store details about deals
    sale:'sale', //used to store details about sales
    dailyentry:'dailyentry' //used to store the daily details for supervisors
};
 
module.exports = collections;
