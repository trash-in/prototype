/**
 * @project trashin_prototype
 * @file   /app/model/person.js
 * @author  Prathik Kaliyambath
 * @author  Manishanker Talusani
 * @version 0.0.1 
 *
 * @section LICENSE
 *
 * Copyright (c) 2016-2017 TrashIn.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 *  The person.js provides schema of a node named person.
 *
 *
 */
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var uri = 'mongodb://127.0.0.1:27017/trashdb'; 
var state = {
    db: null
};
 
exports.connect = function(done) {
    if(state.db) return done();
 
    MongoClient.connect(uri, function(err, db) {
        if (err) {
            console.log("Can't connect to mongo READ!");
            console.log(err);
            return done(err);
        }
 
        state.db = db;
        console.log('Connected to Mongo READ.');
        console.log('Database Name:'+db.databaseName);
        done();
        });
};
 
exports.get = function() {
    return state.db;
};
 
exports.close = function(done) {
    if (state.db) {
        state.db.close(function(err, result) {
            state.db = null;
            done(err);
        });
    }
};
