var utilities = function(){};

var db = require('../database');
var coll = require('../database/collections');

utilities.generateId=function(){
    var newdate = new Date().getTime();
    newdate1 = newdate*1000 + Math.floor(Math.random() * 999);
    return String(newdate1);
}

utilities.generateEmpId = function(cb){
	db.get().collection(coll.counters).findOneAndUpdate({"_id":"empId"},{$inc:{"count":1}},function(err,result){
		cb(err,result.value.count);
	});
}

module.exports = utilities;